#include "Plane3D.h"
#include "Vector3.h"
#include <iostream>
#include <string>
#include <vector>

int main(int argc, char *argv[]) {
  if (argc < 10) {
    return -1;
  }
  std::vector<Vector3> vs;
  vs.resize(3);
  for (int i = 0; i < 3; ++i) {
    int j = i * 3 + 1;
    vs[i] = Vector3(std::stof(argv[j]), std::stof(argv[j + 1]),
                    std::stof(argv[j + 2]));
    std::cout << "vs[" << i << "] = " << vs[i].data()[0] << ","
              << vs[i].data()[1] << "," << vs[i].data()[2] << std::endl;
  }
  Plane3D plane(vs[0], vs[1], vs[2]);
  uint32_t a, b, c, d;
  plane.encode(a, b, c, d);
  std::cout << "M561 A" << a << " B" << b << " C" << c << " D" << d
            << std::endl;
  return 0;
}
