
A simple tool to manually level a smoothieware 3d printer using 3 point calibration. Please bear in mind that I've only run this with my cetus3d with a tinyfab CPU so YMMV. Also, this program comes with no guarantee of fitness for use. So use at your own risk.

## Usage

This requires you to manually figure out the z values for the 3 pre-determined points and then feed the points to this program which creates the correct `M561` command. This command can then be sent to the 3d printer to set up it's surface plane.

First you will first need to turn on zprobe and 3 point calibration (Only so that the `M561`, `M500` and `M503` commands work):

```
zprobe.enable true
leveling-strategy-three-point-leveling.enable true
leveling-strategy-three-point-leveling.point1 90.0,0.0
leveling-strategy-three-point-leveling.point2 180.0,180.0
leveling-strategy-three-point-leveling.point3 0.0,180.0
leveling-strategy-three-point-leveling.save_panel true  # Needed if you want to save the config settings with M500 or M503 commands
```

For instance I used the following 3 points on my cetus3d printer:

```
P0 = 90, 0
P1 = 180, 180
P2 = 0, 180
```

I found acceptable z values for these points to be:

```
Z0 = 0.5
Z1 = 1.38
Z2 = 1.08
```

So I ran the following:

`calibrate 90 0 0.5 180 180 1.38 0 180 1.08`

And the output was:

`M561 A3134878592 B3146048547 C1065353055 D3199414980`

I then started up the 3d printer and sent the above command and then sent M500 to save this value. You can see these settings in config-overrides in the printer's SD card after a printer restart.

## Compiling

```
mkdir build

cd build

cmake ..

make
```

## Additional notes
 * Running M561 without parameters clears the calibration. 
* Remember that G32 starts the automatic calibration routine which you dont want to run unless you actually have a probe sensor.

## References

The calibration code uses code from [smoothieware](https://github.com/Smoothieware/Smoothieware).
The code in question is the Plane3D and Vector3 classes as well as an interpretation of the z-probe module code.


